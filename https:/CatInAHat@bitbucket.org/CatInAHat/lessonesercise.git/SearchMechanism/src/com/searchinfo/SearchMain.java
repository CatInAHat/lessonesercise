package com.searchinfo;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.searchinfo.Car;
import com.searchinfo.ReadCarInfo;

public class SearchMain {

	public static void main(String[] args) {
		
		Date date = new Date();
		SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
		
		ReadCarInfo info = new ReadCarInfo();
	 	Car Car1 = info.car("BMW", 10000.99, 5000.12, 1994,"Pomorskie", "Gdynia", "brak", "tak", ft.format(date));
	 
	 	Car1.CarInfo();
 }

}
